function addJarToJavaClassPath(jarFileName)
% Add a .jar file to the Java Class Path if missing.
%
% INPUT
%
%    jarFileName: full path to the .JAR file
%
% OUTPUT
%
%    None
%
% Aaron Ponti, 2018/06/15

% Check input argument
if nargin ~= 1
    error('One input argument expected.');
end

% Add if the JAR file is not yet in the Java class path
if isempty(find(contains(javaclasspath(), jarFileName), 1))
    javaaddpath(jarFileName);
end
