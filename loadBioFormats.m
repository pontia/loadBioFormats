function varargout = loadBioFormats(varargin)
% loadBioFormats is a wrapper around the Java Bio-Formats library 
%
% https://www.openmicroscopy.org/bio-formats/
%
% SYNOPSIS (1) version = loadBioFormats('--version')
%          (2) [dataset, metadata, success, errMessage] = loadBioFormats(...
%                        fileName, seriesNumber, metadataOnly)
%
% INPUT      SYNOPSIS (1):
%
%            '--version' : return the version of the bio-formats library.
%
%            SYNOPSIS (2):
%
%            filename      String containing the full name with path.
%            seriesNumber  (optional), default: []. In case of files with
%                          more than one series, specify which one to read:
%                          scalars and vectors are supported. Set to Inf to
%                          read all series.
%                          The first series has number = 1!
%                          If seriesNumber is [], a dialog will pop up with
%                          the choice of the series to pick.
%            metadataOnly  {0|1}, default: 0. If 1, only the metadata is
%                          returned.
%
% OUTPUT     dataset       cell array dataset{timepoints, channels} with
%                          all stacks.
%            metadata      structure containing selected metadata extracted
%                          from the OME XML metadata
%            success       1 if success, 0 otherwise.
%            errMessage    in case the bio-formats library fails reading
%                          file,  errMessage contains the text of the
%                          thrown exception. 
%
% Aaron Ponti, 2018/06/15: Initial implementation based on the old
%                          loadGeneric.m function (part of Qu for MATLAB).

% Make sure the BioFormats JAR file is in the Java class path
addJarToJavaClassPath([fileparts(mfilename('fullpath')), ...
    filesep, 'bioformats_package.jar']);

import org.apache.log4j.Logger;
import loci.formats.*;

% Turn of logging
Logger.getRootLogger().removeAllAppenders();
Logger.getRootLogger().addAppender(org.apache.log4j.varia.NullAppender());

% Check if we just need to return the bio-formats version
if nargin == 1 && strcmp(varargin{1}, '--version')
    varargout{1} = char(loci.formats.FormatTools.VERSION);
    return;
end
    
% Default values
seriesNumber = [];
metadataOnly = 0;
        
% Check inputs
switch nargin
    case 1
        fileName = varargin{1};
    case 2
        fileName = varargin{1};
        seriesNumber = varargin{2};
    case 3
        fileName = varargin{1};
        seriesNumber = varargin{2};
        metadataOnly = varargin{3};
        
    otherwise
        error('1, 2, or 3 input parameters expected.');
end

% Initialize output parameters
varargout{1} = {}; % dataset
varargout{2} = []; % metadata
varargout{3} = 0;  % success
varargout{4} = ''; % errMessage

% Create the reader
reader = loci.formats.ChannelSeparator(loci.formats.ChannelFiller());

% Control the handling of files with similar names
%reader = loci.formats.FileStitcher(reader);
reader.setGroupFiles(0);

% Add metadata container
OMEXMLService = loci.formats.services.OMEXMLServiceImpl();
reader.setMetadataStore(OMEXMLService.createOMEXMLMetadata());

% Set the file name
try
    reader.setId(fileName);
catch e
    if isa(e, 'MException')
        varargout{4} = e.message;
    elseif isa(e, 'ExceptionObject')
        varargout{4} = e.ExceptionObject.getMessage();
    else
        varargout{4} = e;
    end
    return;
end

% Get a reference to the metadata store
metadataStore = reader.getMetadataStore();

% Which series to read?
nSeries = reader.getSeriesCount();

if isempty(seriesNumber)
    if nSeries > 1
        choices = cell(1, nSeries);
        for i = 1 : nSeries
            choices{i} = ['Series ',num2str(i)];
        end
        [s, v] = listdlg('PromptString', 'Select the series to open:',...
            'SelectionMode', 'single', 'ListString', choices);
        if v == 0
            reader.close();
            return;
        end
        seriesNumber = s;
    else
        seriesNumber = 1;
    end
else
    if ~isnumeric(seriesNumber)
        varargout{4} = 'The series number must be a number!';
        reader.close();
        return;
    end
    if isscalar(seriesNumber)
        if seriesNumber < 1
            varargout{4} ='The series number cannot be less than 1!';
            reader.close();
            return;
        elseif seriesNumber == Inf
            % Read them all
            seriesNumber = 1 : nSeries;
        elseif seriesNumber > nSeries
            if nSeries == 1
                varargout{4} ='The file contains only one series!';
                reader.close();
                return;
            else
                varargout{4} = ['Please specify a series number between ', ...
                    '1 and ', num2str(nSeries), '!'];
                reader.close();
                return;
            end
        end
    else
        if max(seriesNumber) > nSeries
            varargout{4} = 'The requested series numbers are out of bounds!';
            reader.close();
            return;
        end
    end
end

% Make the seriesNumber index 0-based
seriesNumber = seriesNumber - 1;

% Number of series to process
numSeriesToProcess = numel(seriesNumber);

% Allocate space for the results
seriesDataset = cell(1, numSeriesToProcess);
seriesMetadata = cell(1, numSeriesToProcess);

% Start processing

currentSeriesCounter = 0;
for currentSeries = seriesNumber
    
    % Set the reader onto the selected series
    reader.setSeries(currentSeries);
    
    % Get sizes
    X = reader.getSizeX();
    Y = reader.getSizeY();
    Z = reader.getSizeZ();
    C = reader.getSizeC();
    T = reader.getSizeT();
    N = reader.getImageCount();

    % Get the pixel type
    pixelType = reader.getPixelType();
    
    % Is the datatype signed?
    isSigned = loci.formats.FormatTools.isSigned(pixelType);
    
    % Endianity
    isLittleEndian = reader.isLittleEndian();
    
    % Bytes per pixel
    BytesPerPixel = loci.formats.FormatTools.getBytesPerPixel(pixelType);
    switch BytesPerPixel
        case 1
            datatype = 'uint8';
        case 2
            datatype = 'uint16';
        case 4
            % This is 32-bit floating point
            datatype = 'single';
        otherwise
            varargout{4} = 'Unsupported data type.';
            reader.close();
            return
    end
    
    % =========================================================================
    %
    % Get metadata
    %
    % =========================================================================
    
    % Voxel size X
    voxelX = reader.getMetadataStore.getPixelsPhysicalSizeX(currentSeries);
    if isempty(voxelX)
        voxelX = 0;
    else
        voxelX = double(voxelX.value());
    end
    
    % Voxel size Y
    voxelY = reader.getMetadataStore.getPixelsPhysicalSizeY(currentSeries);
    if isempty(voxelY)
        voxelY = 0;
    else
        voxelY = double(voxelY.value());
    end
    
    % Workaround for 2-D ND2 files
    if voxelY == 0 && voxelX > 0
        voxelY = voxelX;
    end

    % Voxel size Z
    voxelZ = reader.getMetadataStore.getPixelsPhysicalSizeZ(currentSeries);
    if isempty(voxelZ)
        voxelZ = 0;
    else
        voxelZ = double(voxelZ.value());
    end
    voxels = [voxelX voxelY voxelZ];
    
    % Emission and excitation wavelwngths
    [emWL, excWL] = getChannelWavelengths();
    
    % Acquisition date
    acqDateObj = metadataStore.getImageAcquisitionDate(currentSeries);
    if isempty(acqDateObj)
        acqDate = '';
    else
        acqDate = char(acqDateObj.toString());
    end
    
    % Time interval
    timeInvervalObj = metadataStore.getPixelsTimeIncrement(currentSeries);
    if isempty(timeInvervalObj)
        timeInterval = 0;
    else
        timeInterval = double(timeInvervalObj.value());
    end
    
    metadata = struct(...
        'seriesNumber',          currentSeries + 1, ... % MATLAB 1-based array
        'width',                 reader.getSizeX(), ...
        'height',                reader.getSizeY(), ...
        'nPlanes',               reader.getSizeZ(), ...
        'nChannels',             reader.getSizeC(), ...
        'nTimepoints',           reader.getSizeT(), ...
        'nImagesInSeries',       reader.getImageCount(), ...
        'voxelX',                voxels(1), ...
        'voxelY',                voxels(2), ...
        'voxelZ',                voxels(3), ...
        'datatype',              datatype, ...
        'isThumbnail',           reader.isThumbnailSeries(), ...
        'stagePosition',         getStagePosition(), ...
        'acquisitionDate',       acqDate, ...
        'timeInterval',          timeInterval, ...
        'timestamps',            getTimeStamps(), ...
        'colors',                getChannelColors(), ...
        'channelNames',          { getChannelNames() }, ...
        'emissionWaveLengths',   emWL, ...
        'excitationWaveLengths', excWL, ...
        'totalNumSeries',        nSeries);
    
    metadata.datatype = datatype;
    
    % Store the metadata for current series
    currentSeriesCounter = currentSeriesCounter + 1;
    seriesMetadata{currentSeriesCounter} = metadata;
    clear('metadata');

    if metadataOnly == 1
        
        % Skip reading the files
        continue;
        
    end
    
    % =========================================================================
    %
    % Get pixel data
    %
    % =========================================================================
    
    % Initialize the dataset
    dataset = cell(T, C);
    
    % Allocate space for the individual stacks
    stack = zeros([Y X Z], datatype);
    for t = 1 : T
        for c = 1 : C
            dataset{t, c} = stack;
        end
    end
    
    % Global image counter
    n = 0;
    
    % Open a waitbar
    if N > 1
        hWaitbar = waitbar(0, ...
            ['Loading series ', num2str(currentSeries + 1), ...
            ' (', num2str(currentSeriesCounter), ...
            ' of ', num2str(numSeriesToProcess), ')...']);
    end
    
    % Go over all images and arrange them by plane, channel and time index
    switch char(reader.getDimensionOrder())
        case 'XYZCT'
            
            for t = 1 : T
                
                for c = 1 : C
                    
                    for z = 1 : Z
                        
                        % Load the pixel data
                        arr = loci.common.DataTools.makeDataArray(...
                            reader.openBytes(n), ...
                            BytesPerPixel, isSigned, isLittleEndian);
                        
                        
                        % Java does not have explicitly unsigned datatypes
                        if ~isSigned
                            switch class(arr)
                                case 'int8'
                                    arr = typecast(arr, 'uint8');
                                case 'int16'
                                    arr = typecast(arr, 'uint16');
                                case 'single'
                                    % Do nothing
                                otherwise
                                    varargout{1} = {};
                                    varargout{2} = [];
                                    varargout{3} = 0;
                                    varargout{4} = 'Unsupported datatype.';
                                    reader.close();
                                    return;
                            end
                        end
                        
                        % Reshape into 2D
                        arr = reshape(arr, [X Y])';
                        
                        % Store the plane
                        dataset{t, c}(:, :, z) = arr;
                        
                        % Update the global image counter
                        n = n + 1;
                        
                        if N > 1
                            % Update waitbar
                            waitbar(n / N, hWaitbar);
                        end
                        
                    end
                    
                end
                
            end
            
        case 'XYCZT'
            
            for t = 1 : T
                
                for z = 1 : Z
                    
                    for c = 1 : C
                        
                        % Load the pixel data
                        arr = loci.common.DataTools.makeDataArray(...
                            reader.openBytes(n), ...
                            BytesPerPixel, isSigned, isLittleEndian);
                        
                        % Java does not have explicitly unsigned datatypes
                        if ~isSigned
                            switch class(arr)
                                case 'int8'
                                    arr = typecast(arr, 'uint8');
                                case 'int16'
                                    arr = typecast(arr, 'uint16');
                                otherwise
                                    varargout{1} = {};
                                    varargout{2} = [];
                                    varargout{3} = 0;
                                    varargout{4} = 'Unsupported datatype.';
                                    reader.close();
                                    return;
                            end
                        end
                        
                        % Reshape into 2D
                        arr = reshape(arr, [X Y])';
                        
                        % Store the plane
                        dataset{t, c}(:, :, z) = arr;
                        
                        % Update the global image counter
                        n = n + 1;
                        
                        if N > 1
                            % Update waitbar
                            waitbar(n / N, hWaitbar);
                        end
                        
                    end
                    
                end
                
            end
            
        case 'XYZTC'
            
            for c = 1 : C
                
                for t = 1 : T
                    
                    for z = 1 : Z
                        
                        % Load the pixel data
                        arr = loci.common.DataTools.makeDataArray(...
                            reader.openBytes(n), ...
                            BytesPerPixel, isSigned, isLittleEndian);
                        
                        % Java does not have explicitly unsigned datatypes
                        if ~isSigned
                            switch class(arr)
                                case 'int8'
                                    arr = typecast(arr, 'uint8');
                                case 'int16'
                                    arr = typecast(arr, 'uint16');
                                otherwise
                                    varargout{1} = {};
                                    varargout{2} = [];
                                    varargout{3} = 0;
                                    varargout{4} = 'Unsupported datatype,';
                                    reader.close();
                                    return;
                            end
                        end
                        
                        % Reshape into 2D
                        arr = reshape(arr, [X Y])';
                        
                        % Store the plane
                        dataset{t, c}(:, :, z) = arr;
                        
                        % Update the global image counter
                        n = n + 1;
                        
                        if N > 1
                            % Update waitbar
                            waitbar(n / N, hWaitbar);
                        end
                        
                    end
                    
                end
                
            end
            
        otherwise
            
            varargout{1} = {};
            varargout{2} = [];
            varargout{3} = 0;
            varargout{4} = ['Dimension order (', ...
                char(reader.getDimensionOrder()), ') not supported.'];
            reader.close();
            
            % Close the waitbar if needed
            if ishandle(hWaitbar)
                close(hWaitbar)
            end
            return
    end
   
    % Store dataset for current series
    seriesDataset{currentSeriesCounter} = dataset;
    
    if N > 1 && exist('hWaitbar', 'var')
        % Close waitbar
        close(hWaitbar);
    end
    
end

% Close the reader
reader.close();

% Simplify structure
if numSeriesToProcess == 1
    metadata = seriesMetadata{1};
    dataset = seriesDataset{1};
else
    metadata = seriesMetadata;
    dataset = seriesDataset;
end

% Set success to 1
success = 1;

% Now return
varargout{1} = dataset;
varargout{2} = metadata;
varargout{3} = success;
varargout{4} = '';

% =========================================================================
%
% Functions
%
% =========================================================================

% Extract the time stamps for all timepoints
    function timestamps = getTimeStamps()
       
        % Number of time stamps to retrieve
        nTimepoints = reader.getSizeT();
        
        % Allocate space to store the time stamps
        timestamps = zeros(1, nTimepoints);
        
        % Iterate over the keys and get the values
        for tp = 1 : nTimepoints
            
            option = ['timestamp #', num2str(tp - 1)];
            try
                timestamps(tp) = reader.getSeriesMetadataValue(option);
            catch ex  %#ok<NASGU>
                
                try
                    % Try another format
                    strg = sprintf('%%.%dd', length(num2str(nTimepoints)));
                    option = ['timestamp #', sprintf(strg, tp)];
                    timestamps(tp) = reader.getSeriesMetadataValue(option);
                catch
                    timestamps(tp) = NaN;
                end
            end
            
        end
        
    end

% =========================================================================

% Extract the color for all channels
    function colors = getChannelColors()
        
        % Get the metadata store
        metadataStore = reader.getMetadataStore();
        
        nColors = reader.getSizeC();
        colors = zeros(nColors, 4);
        
        for ch = 1 : nColors
            
            try
                color = metadataStore.getChannelColor(currentSeries, ch - 1);
            catch
                color = [];
            end
            if isempty(color)
                colors = [255 255 255 255];
            else
                colors(ch, 1 : 4) = [ ...
                    color.getRed(), color.getGreen(), ...
                    color.getBlue(), color.getAlpha()];
            end
            
        end
        
        % Return colors in the 0 .. 1 range
        if max(colors(:)) <= 255
            colors = colors ./ 255;
        end
        
    end

% =========================================================================

% Extract the wavelengths for all channels
    function [emWavelengths, excWavelengths] = ...
            getChannelWavelengths()
        
        % Get the metadata store
        metadataStore = reader.getMetadataStore();
        
        nColors = reader.getSizeC();
        
        emWavelengths = zeros(1, nColors);
        excWavelengths = zeros(1, nColors);
        
        for ch = 1 : nColors
            
            % Emission wavelength
            try
                em = metadataStore.getChannelEmissionWavelength( ...
                    currentSeries, ch - 1);
            catch
                em = [];
            end
            if isempty(em)
                em = NaN;
            else
                em = double(em.value());
            end
            emWavelengths(1, ch) = em;
            
            % Excitation wavelength
            try
                exc = metadataStore.getChannelExcitationWavelength(...
                    currentSeries, ch - 1);
            catch
                exc = [];
            end
            if isempty(exc)
                exc = NaN;
            else
                exc = double(exc.value());
            end
            excWavelengths(1, ch) = exc;
            
        end
        
    end

% =========================================================================

% Extract the names for all channels
    function channelNames = getChannelNames()
        
        % Get the metadata store
        metadataStore = reader.getMetadataStore();
        
        nColors = reader.getSizeC();
        
        channelNames = cell(1, nColors);
        
        for ch = 1 : nColors
            
            try
                name = metadataStore.getChannelName(currentSeries, ch - 1);
            catch
                name = [];
            end
            if isempty(name)
                name = '';
            end
            channelNames{1, ch} = char(name);
            
        end
        
    end


% =========================================================================

% Extract the stage positions
    function stagePosition = getStagePosition()
        
        
        % Get X and Y position from current series metadata
        try
            x = double(reader.getSeriesMetadataValue('X position').value);
        catch
            x = -1;
        end
        
        try
            y = double(reader.getSeriesMetadataValue('Y position').value);
        catch
            y = -1;
        end
        
        stagePosition = [x y];
        if any(stagePosition == -1)
            stagePosition = [];
        end
        
    end

end
